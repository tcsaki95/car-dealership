package edu.ubbcluj.cardealership.rest.dto.assembler;

import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.rest.dto.RegistrationDTO;

import javax.ejb.Local;

@Local
public interface RegistrationAssembler {

    User createUser(RegistrationDTO registrationDTO);
}
