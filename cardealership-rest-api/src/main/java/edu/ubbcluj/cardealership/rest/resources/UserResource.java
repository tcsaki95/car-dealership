package edu.ubbcluj.cardealership.rest.resources;

import edu.ubbcluj.cardealership.backend.service.ServiceException;
import edu.ubbcluj.cardealership.backend.service.UserService;
import edu.ubbcluj.cardealership.rest.dto.UserDTO;
import edu.ubbcluj.cardealership.rest.dto.assembler.UserAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Stateless
@Path("/users")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class UserResource {

    private static final String COULDN_T_FIND_THE_USERS = "couldn't find the users";

    private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);

    @EJB
    private UserService userService;

    @EJB
    private UserAssembler userAssembler;

    @GET
    public Response getAllUsers() {
        try {

            List<UserDTO> userDTOList = userService.findAllUsers().stream()
                    .map(userAssembler::createUserDTO)
                    .collect(Collectors.toList());

            return Response.ok(userDTOList).build();

        } catch (ServiceException e) {
            LOGGER.error(COULDN_T_FIND_THE_USERS);
            throw new RestApiException(COULDN_T_FIND_THE_USERS, e);
        }
    }
}
