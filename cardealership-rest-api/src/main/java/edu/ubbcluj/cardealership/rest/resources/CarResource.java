package edu.ubbcluj.cardealership.rest.resources;

import edu.ubbcluj.cardealership.rest.dto.CarDTO;
import edu.ubbcluj.cardealership.rest.dto.assembler.CarAssembler;
import edu.ubbcluj.cardealership.backend.service.CarService;
import edu.ubbcluj.cardealership.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Stateless
@Path("/cars")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class CarResource {

    private static final String GETTING_ALL_CARS_FAILED = "Getting all cars failed";
    private static final String GETTING_ALL_CARS_SORTED_FAILED = "Getting all cars sorted failed";

    private static final Logger LOGGER = LoggerFactory.getLogger(CarResource.class);

    @EJB
    private CarService carService;

    @EJB
    private CarAssembler carAssembler;

    @GET
    public Response listAllCars() {
        try {

            List<CarDTO> carDTOList = carService.findAllCars().stream()
                    .map(carAssembler::createCarDTO)
                    .collect(Collectors.toList());

            return Response.ok(carDTOList).build();

        } catch (ServiceException e) {
            LOGGER.error(GETTING_ALL_CARS_FAILED);
            throw new RestApiException(GETTING_ALL_CARS_FAILED, e);
        }
    }

    @GET
    @Path("/sortedByModel")
    public Response listAllCarsSorted(
            @QueryParam("type") String sortingType) {
        try {

            List<CarDTO> carDTOList = carService.findAllCarsSorted(sortingType).stream()
                    .map(carAssembler::createCarDTO)
                    .collect(Collectors.toList());

            return Response.ok(carDTOList).build();

        } catch (ServiceException e) {
            LOGGER.error(GETTING_ALL_CARS_SORTED_FAILED);
            throw new RestApiException(GETTING_ALL_CARS_SORTED_FAILED, e);
        }
    }
}