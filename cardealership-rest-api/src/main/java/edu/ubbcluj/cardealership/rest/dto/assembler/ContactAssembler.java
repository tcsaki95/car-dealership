package edu.ubbcluj.cardealership.rest.dto.assembler;

import edu.ubbcluj.cardealership.backend.model.Contact;
import edu.ubbcluj.cardealership.rest.dto.ContactDTO;

import javax.ejb.Local;

@Local
public interface ContactAssembler {

    Contact createContact(ContactDTO contactDTO);

    ContactDTO createContactDTO(Contact contact);
}
