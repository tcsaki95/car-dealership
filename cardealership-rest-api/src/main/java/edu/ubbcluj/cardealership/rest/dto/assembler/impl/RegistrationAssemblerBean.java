package edu.ubbcluj.cardealership.rest.dto.assembler.impl;

import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.rest.dto.RegistrationDTO;
import edu.ubbcluj.cardealership.rest.dto.assembler.ContactAssembler;
import edu.ubbcluj.cardealership.rest.dto.assembler.RegistrationAssembler;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class RegistrationAssemblerBean implements RegistrationAssembler {

    @EJB
    private ContactAssembler contactAssembler;

    @Override
    public User createUser(RegistrationDTO registrationDTO) {
        User user = new User();

        user.setUsername(registrationDTO.getUsername());
        user.setFullname(registrationDTO.getFullname());
        user.setPassword(registrationDTO.getPassword());
        user.setContact(contactAssembler.createContact(registrationDTO.getContact()));

        return user;
    }
}
