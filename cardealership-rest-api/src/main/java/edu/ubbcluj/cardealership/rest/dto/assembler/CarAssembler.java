package edu.ubbcluj.cardealership.rest.dto.assembler;

import edu.ubbcluj.cardealership.rest.dto.CarDTO;
import edu.ubbcluj.cardealership.backend.model.Car;

import javax.ejb.Local;

@Local
public interface CarAssembler {

    CarDTO createCarDTO(Car car);
}
