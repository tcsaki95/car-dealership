package edu.ubbcluj.cardealership.rest.dto.assembler;

import edu.ubbcluj.cardealership.rest.dto.UserDTO;
import edu.ubbcluj.cardealership.backend.model.User;

import javax.ejb.Local;

@Local
public interface UserAssembler {
    UserDTO createUserDTO(User user);
}
