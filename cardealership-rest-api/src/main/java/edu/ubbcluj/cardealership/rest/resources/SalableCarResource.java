package edu.ubbcluj.cardealership.rest.resources;

import edu.ubbcluj.cardealership.backend.model.Car;
import edu.ubbcluj.cardealership.backend.service.CarService;
import edu.ubbcluj.cardealership.backend.service.ServiceException;
import edu.ubbcluj.cardealership.rest.dto.RestApiMessageDTO;
import edu.ubbcluj.cardealership.rest.dto.SimpleCarDTO;
import edu.ubbcluj.cardealership.rest.dto.assembler.SimpleCarAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Stateful
@Path("/cars")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class SalableCarResource {

    private static final String LOGIN_REQUIRED = "login required";
    private static final String ADDING_NEW_SALABLE_CAR_FOR_USER_FAILED = "adding new salable car for user failed";
    private static final String ADDING_BUYER_FOR_CAR_FAILED = "adding buyer for car failed";

    private static final Logger LOGGER = LoggerFactory.getLogger(SalableCarResource.class);

    @EJB
    private CarService carService;

    @EJB
    private SimpleCarAssembler simpleCarAssembler;

    @Context
    private HttpServletRequest request;

    @POST
    @Path("/add")
    public Response addNewSalableCarToUser(
            SimpleCarDTO simpleCarDTO) {

        HttpSession session = request.getSession();

        if (session.getAttribute("username") == null) {
            LOGGER.error(LOGIN_REQUIRED);
            throw new RestApiException(LOGIN_REQUIRED);
        }

        final Car car = simpleCarAssembler.createCar(simpleCarDTO);

        try {
            final String username = (String) session.getAttribute("username");
            carService.createNewSalableCarForUser(car, username);

            return Response
                    .ok(new RestApiMessageDTO("new car added to user: " + username))
                    .build();
        } catch (ServiceException e) {
            LOGGER.error(ADDING_NEW_SALABLE_CAR_FOR_USER_FAILED);
            throw new RestApiException(ADDING_NEW_SALABLE_CAR_FOR_USER_FAILED, e);
        }
    }

    @POST
    @Path("/buy/{carId}")
    public Response buyCarWithGivenId(
            @PathParam("carId") Long carId
    ) {

        HttpSession session = request.getSession();

        if (session.getAttribute("username") == null) {
            LOGGER.error(LOGIN_REQUIRED);
            throw new RestApiException(LOGIN_REQUIRED);
        }

        try {
            final String username = (String) session.getAttribute("username");
            carService.addBuyerToCar(carId, username);

            return Response
                    .ok(new RestApiMessageDTO("buyer added to car"))
                    .build();
        } catch (ServiceException e) {
            LOGGER.error(ADDING_BUYER_FOR_CAR_FAILED);
            throw new RestApiException(ADDING_BUYER_FOR_CAR_FAILED, e);
        }


    }
}
