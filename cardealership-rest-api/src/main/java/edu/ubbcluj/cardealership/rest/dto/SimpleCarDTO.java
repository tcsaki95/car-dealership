package edu.ubbcluj.cardealership.rest.dto;

public class SimpleCarDTO {

    private String brand;
    private String model;
    private Integer yearOfManifacture;
    private Integer bodyMass;
    private Integer topSpeed;
    private Integer priceInEuro;

    public SimpleCarDTO() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYearOfManifacture() {
        return yearOfManifacture;
    }

    public void setYearOfManifacture(Integer yearOfManifacture) {
        this.yearOfManifacture = yearOfManifacture;
    }

    public Integer getBodyMass() {
        return bodyMass;
    }

    public void setBodyMass(Integer bodyMass) {
        this.bodyMass = bodyMass;
    }

    public Integer getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(Integer topSpeed) {
        this.topSpeed = topSpeed;
    }

    public Integer getPriceInEuro() {
        return priceInEuro;
    }

    public void setPriceInEuro(Integer priceInEuro) {
        this.priceInEuro = priceInEuro;
    }
}
