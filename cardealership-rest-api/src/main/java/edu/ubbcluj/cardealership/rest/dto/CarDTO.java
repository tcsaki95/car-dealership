package edu.ubbcluj.cardealership.rest.dto;

public class CarDTO {

    private Long id;
    private String brand;
    private String model;
    private Integer yearOfManifacture;
    private Integer bodyMass;
    private Integer topSpeed;
    private Integer priceInEuro;
    private SimpleUserDTO seller;
    private SimpleUserDTO buyer;

    public CarDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYearOfManifacture() {
        return yearOfManifacture;
    }

    public void setYearOfManifacture(Integer yearOfManifacture) {
        this.yearOfManifacture = yearOfManifacture;
    }

    public Integer getBodyMass() {
        return bodyMass;
    }

    public void setBodyMass(Integer bodyMass) {
        this.bodyMass = bodyMass;
    }

    public Integer getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(Integer topSpeed) {
        this.topSpeed = topSpeed;
    }

    public Integer getPriceInEuro() {
        return priceInEuro;
    }

    public void setPriceInEuro(Integer priceInEuro) {
        this.priceInEuro = priceInEuro;
    }

    public SimpleUserDTO getSeller() {
        return seller;
    }

    public void setSeller(SimpleUserDTO seller) {
        this.seller = seller;
    }

    public SimpleUserDTO getBuyer() {
        return buyer;
    }

    public void setBuyer(SimpleUserDTO buyer) {
        this.buyer = buyer;
    }
}
