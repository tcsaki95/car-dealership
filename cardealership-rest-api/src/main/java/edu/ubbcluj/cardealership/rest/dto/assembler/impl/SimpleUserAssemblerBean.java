package edu.ubbcluj.cardealership.rest.dto.assembler.impl;

import edu.ubbcluj.cardealership.rest.dto.SimpleUserDTO;
import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.rest.dto.assembler.SimpleUserAssembler;

import javax.ejb.Stateless;

@Stateless
public class SimpleUserAssemblerBean implements SimpleUserAssembler {
    @Override
    public SimpleUserDTO createSellerDTO(User user) {
        SimpleUserDTO simpleUserDTO = new SimpleUserDTO();

        simpleUserDTO.setUsername(user.getUsername());
        simpleUserDTO.setFullname(user.getFullname());

        return simpleUserDTO;
    }
}
