package edu.ubbcluj.cardealership.rest.dto.assembler.impl;

import edu.ubbcluj.cardealership.rest.dto.UserDTO;
import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.rest.dto.assembler.ContactAssembler;
import edu.ubbcluj.cardealership.rest.dto.assembler.SimpleCarAssembler;
import edu.ubbcluj.cardealership.rest.dto.assembler.UserAssembler;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.stream.Collectors;

@Stateless
public class UserAssemblerBean implements UserAssembler {

    @EJB
    private SimpleCarAssembler simpleCarAssembler;

    @EJB
    private ContactAssembler contactAssembler;

    @Override
    public UserDTO createUserDTO(User user) {
        UserDTO userDTO = new UserDTO();

        userDTO.setUsername(user.getUsername());
        userDTO.setFullname(user.getFullname());
        userDTO.setContactDTO(contactAssembler.createContactDTO(user.getContact()));

        userDTO.setCarsToSell(user.getCarsToSell().stream()
                .map(simpleCarAssembler::createSalableCarDTO)
                .collect(Collectors.toList()));
        userDTO.setBoughtCars(user.getBoughtCars().stream()
                .map(simpleCarAssembler::createSalableCarDTO)
                .collect(Collectors.toList()));

        return userDTO;
    }

}
