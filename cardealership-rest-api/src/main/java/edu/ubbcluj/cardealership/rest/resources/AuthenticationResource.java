package edu.ubbcluj.cardealership.rest.resources;

import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.backend.service.AuthenticationService;
import edu.ubbcluj.cardealership.backend.service.ServiceException;
import edu.ubbcluj.cardealership.rest.dto.LoginDTO;
import edu.ubbcluj.cardealership.rest.dto.RegistrationDTO;
import edu.ubbcluj.cardealership.rest.dto.RestApiMessageDTO;
import edu.ubbcluj.cardealership.rest.dto.UserDTO;
import edu.ubbcluj.cardealership.rest.dto.assembler.RegistrationAssembler;
import edu.ubbcluj.cardealership.rest.dto.assembler.UserAssembler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateful;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Stateful
@Path("/users")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public class AuthenticationResource {

    private static final String USER_CREATION_FAILED = "user creation failed";
    private static final String YOU_ARE_ALREADY_LOGGED_IN = "you are already logged in";
    private static final String WRONG_USERNAME_OR_PASSWORD = "wrong username or password";
    private static final String VERIFY_FAILED = "verify failed";
    private static final String THERE_IS_NO_USER_LOGGED_IN = "there is no user logged in";

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationResource.class);

    @EJB
    private AuthenticationService authenticationService;

    @EJB
    private RegistrationAssembler registrationAssembler;

    @EJB
    private UserAssembler userAssembler;

    @POST
    @Path("/register")
    public Response registerUser(RegistrationDTO registrationDTO) {
        try {
            User user = registrationAssembler.createUser(registrationDTO);
            user = authenticationService.register(user);
            UserDTO userDTO = userAssembler.createUserDTO(user);
            return Response
                    .ok(userDTO)
                    .build();
        } catch (ServiceException e) {
            LOGGER.error(USER_CREATION_FAILED);
            throw new RestApiException(USER_CREATION_FAILED, e);
        }
    }

    @POST
    @Path("/login")
    public Response loginUser(@Context HttpServletRequest request,
                              LoginDTO loginDTO) {
        try {
            HttpSession session = request.getSession();
            if (session.getAttribute("username") != null) {
                LOGGER.error(YOU_ARE_ALREADY_LOGGED_IN);
                throw new RestApiException(YOU_ARE_ALREADY_LOGGED_IN);
            }

            if (!authenticationService.verify(loginDTO.getUsername(), loginDTO.getPassword())) {
                LOGGER.error(WRONG_USERNAME_OR_PASSWORD);
                throw new RestApiException(WRONG_USERNAME_OR_PASSWORD);
            }
            session.setAttribute("username", loginDTO.getUsername());

            return Response
                    .ok(new RestApiMessageDTO("you logged in successfully"))
                    .build();
        } catch (ServiceException e) {
            LOGGER.error(VERIFY_FAILED);
            throw new RestApiException(VERIFY_FAILED);
        }
    }

    @POST
    @Path("/logout")
    public Response logoutUser(@Context HttpServletRequest request) {
        HttpSession session = request.getSession();

        if (session.getAttribute("username") == null) {
            LOGGER.error(THERE_IS_NO_USER_LOGGED_IN);
            throw new RestApiException(THERE_IS_NO_USER_LOGGED_IN);
        }
        session.setAttribute("username", null);

        return Response
                .ok(new RestApiMessageDTO("you logged out successfully"))
                .build();
    }
}
