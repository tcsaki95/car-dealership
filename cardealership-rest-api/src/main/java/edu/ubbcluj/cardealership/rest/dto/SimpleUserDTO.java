package edu.ubbcluj.cardealership.rest.dto;

public class SimpleUserDTO {

    private String username;
    private String fullname;

    public SimpleUserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
}
