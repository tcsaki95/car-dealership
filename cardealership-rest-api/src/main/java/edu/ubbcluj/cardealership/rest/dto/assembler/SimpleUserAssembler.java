package edu.ubbcluj.cardealership.rest.dto.assembler;

import edu.ubbcluj.cardealership.rest.dto.SimpleUserDTO;
import edu.ubbcluj.cardealership.backend.model.User;

import javax.ejb.Local;

@Local
public interface SimpleUserAssembler {
    SimpleUserDTO createSellerDTO(User user);
}
