package edu.ubbcluj.cardealership.rest.dto.assembler.impl;

import edu.ubbcluj.cardealership.rest.dto.CarDTO;
import edu.ubbcluj.cardealership.backend.model.Car;
import edu.ubbcluj.cardealership.rest.dto.assembler.CarAssembler;
import edu.ubbcluj.cardealership.rest.dto.assembler.SimpleUserAssembler;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class CarAssemblerBean implements CarAssembler {

    @EJB
    private SimpleUserAssembler simpleUserAssembler;

    @Override
    public CarDTO createCarDTO(Car car) {
        CarDTO carDTO = new CarDTO();

        carDTO.setId(car.getId());
        carDTO.setTopSpeed(car.getTopSpeed());
        carDTO.setPriceInEuro(car.getPriceInEuro());
        carDTO.setModel(car.getModel());
        carDTO.setBrand(car.getBrand());
        carDTO.setBodyMass(car.getBodyMass());
        carDTO.setYearOfManifacture(car.getYearOfManifacture());
        carDTO.setSeller(simpleUserAssembler.createSellerDTO(car.getSeller()));
        carDTO.setBuyer(car.getBuyer() == null ? null : simpleUserAssembler.createSellerDTO(car.getBuyer()));

        return carDTO;
    }

}
