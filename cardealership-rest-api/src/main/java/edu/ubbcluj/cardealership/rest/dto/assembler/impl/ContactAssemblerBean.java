package edu.ubbcluj.cardealership.rest.dto.assembler.impl;

import edu.ubbcluj.cardealership.backend.model.Contact;
import edu.ubbcluj.cardealership.rest.dto.ContactDTO;
import edu.ubbcluj.cardealership.rest.dto.assembler.ContactAssembler;

import javax.ejb.Stateless;

@Stateless
public class ContactAssemblerBean implements ContactAssembler {

    @Override
    public Contact createContact(ContactDTO contactDTO) {
        Contact contact = new Contact();

        contact.setEmail(contactDTO.getEmail());
        contact.setPhoneNumber(contactDTO.getPhoneNumber());

        return contact;
    }

    @Override
    public ContactDTO createContactDTO(Contact contact) {
        ContactDTO contactDTO = new ContactDTO();

        contactDTO.setEmail(contact.getEmail());
        contactDTO.setPhoneNumber(contact.getPhoneNumber());

        return contactDTO;
    }
}
