package edu.ubbcluj.cardealership.rest.dto;

public class RestApiMessageDTO {
    private String message;

    public RestApiMessageDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
