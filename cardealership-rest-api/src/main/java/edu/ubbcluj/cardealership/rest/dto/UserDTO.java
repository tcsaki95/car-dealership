package edu.ubbcluj.cardealership.rest.dto;

import java.util.ArrayList;
import java.util.Collection;

public class UserDTO {

    private String username;
    private String fullname;
    private ContactDTO contactDTO;
    private Collection<SimpleCarDTO> carsToSell = new ArrayList<>();
    private Collection<SimpleCarDTO> boughtCars = new ArrayList<>();


    public UserDTO() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public ContactDTO getContactDTO() {
        return contactDTO;
    }

    public void setContactDTO(ContactDTO contactDTO) {
        this.contactDTO = contactDTO;
    }

    public Collection<SimpleCarDTO> getCarsToSell() {
        return carsToSell;
    }

    public void setCarsToSell(Collection<SimpleCarDTO> carsToSell) {
        this.carsToSell = carsToSell;
    }

    public Collection<SimpleCarDTO> getBoughtCars() {
        return boughtCars;
    }

    public void setBoughtCars(Collection<SimpleCarDTO> boughtCars) {
        this.boughtCars = boughtCars;
    }
}
