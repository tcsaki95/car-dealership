package edu.ubbcluj.cardealership.rest.dto.assembler;

import edu.ubbcluj.cardealership.rest.dto.SimpleCarDTO;
import edu.ubbcluj.cardealership.backend.model.Car;

import javax.ejb.Local;

@Local
public interface SimpleCarAssembler {

    SimpleCarDTO createSalableCarDTO(Car car);

    Car createCar(SimpleCarDTO simpleCarDTO);
}
