package edu.ubbcluj.cardealership.rest.dto.assembler.impl;

import edu.ubbcluj.cardealership.rest.dto.SimpleCarDTO;
import edu.ubbcluj.cardealership.backend.model.Car;
import edu.ubbcluj.cardealership.rest.dto.assembler.SimpleCarAssembler;

import javax.ejb.Stateless;

@Stateless
public class SimpleCarAssemblerBean implements SimpleCarAssembler {

    @Override
    public SimpleCarDTO createSalableCarDTO(Car car) {
        SimpleCarDTO simpleCarDTO = new SimpleCarDTO();

        simpleCarDTO.setTopSpeed(car.getTopSpeed());
        simpleCarDTO.setPriceInEuro(car.getPriceInEuro());
        simpleCarDTO.setModel(car.getModel());
        simpleCarDTO.setBrand(car.getBrand());
        simpleCarDTO.setBodyMass(car.getBodyMass());
        simpleCarDTO.setYearOfManifacture(car.getYearOfManifacture());

        return simpleCarDTO;
    }

    @Override
    public Car createCar(SimpleCarDTO simpleCarDTO) {
        Car car = new Car();

        car.setTopSpeed(simpleCarDTO.getTopSpeed());
        car.setPriceInEuro(simpleCarDTO.getPriceInEuro());
        car.setModel(simpleCarDTO.getModel());
        car.setBrand(simpleCarDTO.getBrand());
        car.setBodyMass(simpleCarDTO.getBodyMass());
        car.setYearOfManifacture(simpleCarDTO.getYearOfManifacture());

        return car;
    }
}
