package edu.ubbcluj.cardealership.rest.resources;

import edu.ubbcluj.cardealership.rest.dto.RestApiMessageDTO;

import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Provider
public class RestApiExceptionMapper implements ExceptionMapper<Throwable> {

    @Override
    @Produces(APPLICATION_JSON)
    public Response toResponse(Throwable exception) {
        return Response
                .serverError()
                .entity(new RestApiMessageDTO(exception.getMessage()))
                .build();
    }
}
