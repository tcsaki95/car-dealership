package edu.ubbcluj.cardealership.backend.repository.jpa;

import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.backend.repository.RepositoryException;
import edu.ubbcluj.cardealership.backend.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class UserJpaRepository extends JpaRepository<User> implements UserRepository {

    private static final String ERROR_WHILE_FINDING_A_USER_BY_USERNAME = "error while finding a user by username";
    private static final String ERROR_WHILE_FINDING_USER_WITH_USERNAME = "error while finding user with username: ";
    private static final String ERROR_WHILE_GETTING_USERS_WITH_USERNAME = "error while getting users with username";

    private static final Logger LOGGER = LoggerFactory.getLogger(UserJpaRepository.class);

    public UserJpaRepository() {
        super(User.class);
    }

    @Override
    public boolean userExists(String username) {
        try {
            final List<User> resultList = getUsers(username);
            return resultList.size() != 0;
        } catch (PersistenceException e) {
            LOGGER.error(ERROR_WHILE_FINDING_A_USER_BY_USERNAME);
            throw new RepositoryException(ERROR_WHILE_FINDING_A_USER_BY_USERNAME, e);
        }
    }

    @Override
    public User findUserByUsername(String username) throws RepositoryException {
        try {
            final List<User> resultList = getUsers(username);
            if (resultList.size() == 0) {
                LOGGER.error("user doesn't exist with user name: " + username);
                throw new RepositoryException("user doesn't exist with user name: " + username);
            }
            return resultList.get(0);
        } catch (PersistenceException e) {
            LOGGER.error(ERROR_WHILE_FINDING_USER_WITH_USERNAME + username);
            throw new RepositoryException(ERROR_WHILE_FINDING_USER_WITH_USERNAME + username, e);
        }
    }

    private List<User> getUsers(String username) throws PersistenceException {
        try {
            final TypedQuery<User> namedQuery = entityManager.createNamedQuery(User.FIND_USER_BY_USERNAME, User.class);
            namedQuery.setParameter("username", username);
            return namedQuery.getResultList();
        } catch (Exception e) {
            LOGGER.error(ERROR_WHILE_GETTING_USERS_WITH_USERNAME);
            throw new RepositoryException(ERROR_WHILE_GETTING_USERS_WITH_USERNAME, e);
        }
    }
}
