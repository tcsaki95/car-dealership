package edu.ubbcluj.cardealership.backend.service;

import edu.ubbcluj.cardealership.backend.model.User;

import javax.ejb.Local;
import java.util.List;

@Local
public interface UserService {

    List<User> findAllUsers() throws ServiceException;
}
