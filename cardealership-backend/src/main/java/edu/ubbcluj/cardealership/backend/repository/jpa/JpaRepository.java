package edu.ubbcluj.cardealership.backend.repository.jpa;

import edu.ubbcluj.cardealership.backend.model.BaseEntity;
import edu.ubbcluj.cardealership.backend.repository.Repository;
import edu.ubbcluj.cardealership.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

public abstract class JpaRepository<T extends BaseEntity> implements Repository<T> {

    private static final String ERROR_WHILE_FINDING_ALL_ENTITIES = "error while finding all entities";
    private static final String FIND_FAILED = "find failed";
    private static final String CREATE_FAILED = "create failed";

    private static final Logger LOGGER = LoggerFactory.getLogger(JpaRepository.class);

    @PersistenceContext(unitName = "carDealershipPU")
    protected EntityManager entityManager;

    private final Class<T> entityClass;

    JpaRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public List<T> findAll() {
        try {
            final TypedQuery<T> query = entityManager.createQuery("select e from " + entityClass.getSimpleName() + " e", entityClass);
            return query.getResultList();
        } catch (PersistenceException e) {
            LOGGER.error(ERROR_WHILE_FINDING_ALL_ENTITIES);
            throw new RepositoryException(ERROR_WHILE_FINDING_ALL_ENTITIES, e);
        }
    }

    @Override
    public T findById(Long id) {
        try {
            return entityManager.find(entityClass, id);
        } catch (IllegalArgumentException ex) {
            LOGGER.error(FIND_FAILED);
            throw new RepositoryException(FIND_FAILED, ex);
        }
    }

    @Override
    @Transactional
    public T create(T t) {
        try {
            return entityManager.merge(t);
        } catch (PersistenceException | IllegalArgumentException ex) {
            LOGGER.error(CREATE_FAILED);
            throw new RepositoryException(CREATE_FAILED, ex);
        }
    }
}
