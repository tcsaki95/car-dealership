package edu.ubbcluj.cardealership.backend.repository;

import edu.ubbcluj.cardealership.backend.model.User;

import javax.ejb.Local;

@Local
public interface UserRepository extends Repository<User> {

    boolean userExists(String username) throws RepositoryException;

    User findUserByUsername(String username) throws RepositoryException;
}
