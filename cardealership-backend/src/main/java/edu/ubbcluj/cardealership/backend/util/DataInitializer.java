package edu.ubbcluj.cardealership.backend.util;

import edu.ubbcluj.cardealership.backend.model.Car;
import edu.ubbcluj.cardealership.backend.model.Contact;
import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.backend.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Singleton
@Startup
public class DataInitializer {

    @EJB
    private UserRepository userRepository;

    @EJB
    private PasswordEncrypter passwordEncrypter;

    @PostConstruct
    public void initialize() {
        Random random = new Random();

        List<Car> carList = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            Car car = new Car();
            car.setBodyMass(1000 + random.nextInt(1500));
            car.setBrand("TestBrand-" + random.nextInt(50));
            car.setModel("TestModel-" + random.nextInt(50));
            car.setPriceInEuro(500 + random.nextInt(2000));
            car.setTopSpeed(100 + random.nextInt(150));
            car.setYearOfManifacture(1980 + random.nextInt(36));

            carList.add(car);
        }

        User user = new User();
        user.setUsername("tcsaki");
        user.setFullname("Tamas Csaki");
        user.setPassword(passwordEncrypter.createHash("123-456"));

        Contact contact = new Contact();
        contact.setEmail("csaki.tamas95@gmail.com");
        contact.setPhoneNumber("0735-693-010");

        user.setContact(contact);

        carList.forEach(user::addCarToSell);

        userRepository.create(user);
    }
}
