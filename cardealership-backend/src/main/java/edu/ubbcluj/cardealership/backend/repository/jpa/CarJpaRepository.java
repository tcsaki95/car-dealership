package edu.ubbcluj.cardealership.backend.repository.jpa;

import edu.ubbcluj.cardealership.backend.model.Car;
import edu.ubbcluj.cardealership.backend.repository.CarRepository;
import edu.ubbcluj.cardealership.backend.repository.RepositoryException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class CarJpaRepository extends JpaRepository<Car> implements CarRepository {

    private static final String ERROR_WHILE_FINDING_ALL_CARS_SORTED_ASC_BY_MODEL_NAME = "error while finding all cars sorted (asc) by model name";
    private static final String ERROR_WHILE_FINDING_ALL_CARS_SORTED_DESC_BY_MODEL_NAME = "error while finding all cars sorted (desc) by model name";

    private static final Logger LOGGER = LoggerFactory.getLogger(CarJpaRepository.class);

    public CarJpaRepository() {
        super(Car.class);
    }

    @Override
    public List<Car> findAllCarsSortedAsc() throws RepositoryException {
        try {
            final TypedQuery<Car> namedQuery = entityManager.createNamedQuery(Car.FIND_ALL_CARS_SORTED_BY_NAME_ASC, Car.class);
            return namedQuery.getResultList();
        } catch (PersistenceException e) {
            LOGGER.error(ERROR_WHILE_FINDING_ALL_CARS_SORTED_ASC_BY_MODEL_NAME);
            throw new RepositoryException(ERROR_WHILE_FINDING_ALL_CARS_SORTED_ASC_BY_MODEL_NAME, e);
        }
    }

    @Override
    public List<Car> findAllCarsSortedDesc() throws RepositoryException {
        try {
            final TypedQuery<Car> namedQuery = entityManager.createNamedQuery(Car.FIND_ALL_CARS_SORTED_BY_NAME_DESC, Car.class);
            return namedQuery.getResultList();
        } catch (PersistenceException e) {
            LOGGER.error(ERROR_WHILE_FINDING_ALL_CARS_SORTED_DESC_BY_MODEL_NAME);
            throw new RepositoryException(ERROR_WHILE_FINDING_ALL_CARS_SORTED_DESC_BY_MODEL_NAME, e);
        }
    }
}
