package edu.ubbcluj.cardealership.backend.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@NamedQueries({
        @NamedQuery(name = User.FIND_USER_BY_USERNAME, query = "select u from User u where u.username = :username")
})
public class User extends BaseEntity {

    public static final String FIND_USER_BY_USERNAME = "User.findByUsername";

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String fullname;

    @Column(nullable = false)
    private String password;

    @OneToOne(
            cascade = CascadeType.ALL
    )
    private Contact contact;

    @OneToMany(
            mappedBy = "seller",
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE,
                    CascadeType.REFRESH
            },
            orphanRemoval = true
    )
    private Collection<Car> carsToSell = new ArrayList<>();

    @OneToMany(
            mappedBy = "buyer",
            cascade =  {
                    CascadeType.PERSIST,
                    CascadeType.MERGE,
                    CascadeType.REFRESH
            },
            orphanRemoval = true
    )
    private Collection<Car> boughtCars = new ArrayList<>();

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Collection<Car> getCarsToSell() {
        return carsToSell;
    }

    public void addCarToSell(Car car) {
        carsToSell.add(car);
        car.setSeller(this);
    }

    public Collection<Car> getBoughtCars() {
        return boughtCars;
    }

    public void ownCar(Car car) {
        boughtCars.add(car);
        car.setBuyer(this);
    }
}
