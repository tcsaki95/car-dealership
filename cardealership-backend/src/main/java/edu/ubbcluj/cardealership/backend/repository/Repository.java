package edu.ubbcluj.cardealership.backend.repository;

import edu.ubbcluj.cardealership.backend.model.BaseEntity;

import java.util.List;

public interface Repository<T extends BaseEntity> {
    List<T> findAll();

    T findById(Long id);

    T create(T t);
}
