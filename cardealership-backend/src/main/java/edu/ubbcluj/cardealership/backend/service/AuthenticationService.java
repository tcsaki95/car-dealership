package edu.ubbcluj.cardealership.backend.service;

import edu.ubbcluj.cardealership.backend.model.User;

import javax.ejb.Local;

@Local
public interface AuthenticationService {

    User register(User user) throws ServiceException;

    boolean verify(String username, String password) throws ServiceException;
}
