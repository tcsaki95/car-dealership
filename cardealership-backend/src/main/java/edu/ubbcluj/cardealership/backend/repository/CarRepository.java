package edu.ubbcluj.cardealership.backend.repository;

import edu.ubbcluj.cardealership.backend.model.Car;

import javax.ejb.Local;
import java.util.List;

@Local
public interface CarRepository extends Repository<Car> {

    List<Car> findAllCarsSortedAsc() throws RepositoryException;

    List<Car> findAllCarsSortedDesc() throws RepositoryException;
}
