package edu.ubbcluj.cardealership.backend.service.impl;

import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.backend.repository.UserRepository;
import edu.ubbcluj.cardealership.backend.service.ServiceException;
import edu.ubbcluj.cardealership.backend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class UserServiceBean implements UserService {

    private static final String ERROR_WHILE_FINDING_THE_USERS = "error while finding the users";

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceBean.class);

    @EJB
    private UserRepository userRepository;

    @Override
    public List<User> findAllUsers() throws ServiceException {
        try {
            return userRepository.findAll();
        } catch (Exception e) {
            LOGGER.error(ERROR_WHILE_FINDING_THE_USERS);
            throw new ServiceException(ERROR_WHILE_FINDING_THE_USERS, e);
        }
    }
}
