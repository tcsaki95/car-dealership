package edu.ubbcluj.cardealership.backend.service.impl;

import edu.ubbcluj.cardealership.backend.model.Car;
import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.backend.repository.CarRepository;
import edu.ubbcluj.cardealership.backend.repository.RepositoryException;
import edu.ubbcluj.cardealership.backend.repository.UserRepository;
import edu.ubbcluj.cardealership.backend.service.CarService;
import edu.ubbcluj.cardealership.backend.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class CarServiceBean implements CarService {

    private static final String FIND_ALL_CARS_FAILED = "findAllCars failed";
    private static final String FIND_ALL_CARS_SORTED_FAILED = "findAllCarsSorted failed";
    private static final String CREATE_NEW_SALABLE_CAR_FOR_USER_FAILED = "createNewSalableCarForUser failed";
    private static final String ADD_BUYER_TO_CAR_FAILED = "addBuyerToCar failed";
    private static final String YOU_ARE_TRYING_TO_BUY_YOUR_OWN_CAR_IT_ISN_T_WORKING = "you are trying to buy your own car...it isn't working";

    private static final Logger LOGGER = LoggerFactory.getLogger(CarServiceBean.class);

    @EJB
    private CarRepository carRepository;

    @EJB
    private UserRepository userRepository;

    @Override
    public List<Car> findAllCars() throws ServiceException {
        try {
            return carRepository.findAll();
        } catch (RepositoryException e) {
            LOGGER.error(FIND_ALL_CARS_FAILED, e);
            throw new ServiceException(FIND_ALL_CARS_FAILED, e);
        }
    }

    @Override
    public List<Car> findAllCarsSorted(String sortingType) throws ServiceException {
        try {
            List<Car> carList;

            switch (sortingType.toLowerCase()) {
                case "asc": carList = carRepository.findAllCarsSortedAsc(); break;
                case "desc": carList = carRepository.findAllCarsSortedDesc(); break;
                default: carList = carRepository.findAll();
            }

            return carList;
        } catch (RepositoryException e) {
            LOGGER.error(FIND_ALL_CARS_SORTED_FAILED, e);
            throw new ServiceException(FIND_ALL_CARS_SORTED_FAILED, e);
        }
    }

    @Override
    public void createNewSalableCarForUser(Car car, String username) throws ServiceException {
        try {
            final User userByUsername = userRepository.findUserByUsername(username);

            userByUsername.addCarToSell(car);
        } catch (RepositoryException e) {
            LOGGER.error(CREATE_NEW_SALABLE_CAR_FOR_USER_FAILED, e);
            throw new ServiceException(CREATE_NEW_SALABLE_CAR_FOR_USER_FAILED, e);
        }
    }

    @Override
    public void addBuyerToCar(Long carId, String username) throws ServiceException {
        try {
            final User user = userRepository.findUserByUsername(username);
            final Car car = carRepository.findById(carId);

            if (car == null) {
                LOGGER.error("car with id: " + carId + " doesn't exist");
                throw new ServiceException("car with id: " + carId + " doesn't exist");
            }

            if (car.getBuyer() != null) {
                LOGGER.error("the car with id: " + carId + " is already sold");
                throw new ServiceException("the car with id: " + carId + " is already sold");
            }

            if (car.getSeller().getUsername().equals(username)) {
                LOGGER.error(YOU_ARE_TRYING_TO_BUY_YOUR_OWN_CAR_IT_ISN_T_WORKING);
                throw new ServiceException(YOU_ARE_TRYING_TO_BUY_YOUR_OWN_CAR_IT_ISN_T_WORKING);
            }

            car.setBuyer(user);
        } catch (RepositoryException e) {
            LOGGER.error(ADD_BUYER_TO_CAR_FAILED);
            throw new ServiceException(ADD_BUYER_TO_CAR_FAILED, e);
        }
    }
}
