package edu.ubbcluj.cardealership.backend.service.impl;

import edu.ubbcluj.cardealership.backend.model.User;
import edu.ubbcluj.cardealership.backend.repository.RepositoryException;
import edu.ubbcluj.cardealership.backend.repository.UserRepository;
import edu.ubbcluj.cardealership.backend.service.AuthenticationService;
import edu.ubbcluj.cardealership.backend.service.ServiceException;
import edu.ubbcluj.cardealership.backend.util.PasswordEncrypter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.EJB;
import javax.ejb.Stateful;

@Stateful
public class AuthenticationServiceBean implements AuthenticationService {

    private static final String WRONG_USERNAME_OR_PASSWORD = "wrong username or password";
    private static final String USER_REGISTRATION_FAILED = "User registration failed";
    private static final String USER_REGISTRATION_FAILED_USERNAME_EXISTS = "User registration failed: username exists";

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationServiceBean.class);

    @EJB
    private UserRepository userRepository;

    @EJB
    private PasswordEncrypter passwordEncrypter;

    @Override
    public User register(User user) throws ServiceException {
        try {
            if (userRepository.userExists(user.getUsername())) {
                LOGGER.error(USER_REGISTRATION_FAILED_USERNAME_EXISTS);
                throw new ServiceException(USER_REGISTRATION_FAILED_USERNAME_EXISTS);
            }
            user.setPassword(passwordEncrypter.createHash(user.getPassword()));
            return userRepository.create(user);
        } catch (RepositoryException e) {
            LOGGER.error(USER_REGISTRATION_FAILED);
            throw new ServiceException(USER_REGISTRATION_FAILED, e);
        }
    }

    @Override
    public boolean verify(String username, String password) throws ServiceException {
        User userByUsername;
        try {
            userByUsername = userRepository.findUserByUsername(username);
        } catch (RepositoryException e) {
            LOGGER.error(WRONG_USERNAME_OR_PASSWORD);
            throw new ServiceException(WRONG_USERNAME_OR_PASSWORD);
        }
        return passwordEncrypter.validatePassword(password, userByUsername.getPassword());
    }

}
