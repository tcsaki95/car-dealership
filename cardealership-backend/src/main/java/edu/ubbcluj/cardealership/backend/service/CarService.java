package edu.ubbcluj.cardealership.backend.service;

import edu.ubbcluj.cardealership.backend.model.Car;

import javax.ejb.Local;
import java.util.List;

@Local
public interface CarService {

    List<Car> findAllCars() throws ServiceException;

    List<Car> findAllCarsSorted(String sortingType) throws ServiceException;

    void createNewSalableCarForUser(Car car, String username) throws ServiceException;

    void addBuyerToCar(Long carId, String username) throws ServiceException;
}
