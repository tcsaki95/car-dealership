package edu.ubbcluj.cardealership.backend.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
        @NamedQuery(name = Car.FIND_ALL_CARS_SORTED_BY_NAME_ASC, query = "select c from Car c order by c.model asc"),
        @NamedQuery(name = Car.FIND_ALL_CARS_SORTED_BY_NAME_DESC, query = "select c from Car c order by c.model desc")
})
public class Car extends BaseEntity {

    public static final String FIND_ALL_CARS_SORTED_BY_NAME_ASC = "Car.findAllCarsSortedByNameAsc";
    public static final String FIND_ALL_CARS_SORTED_BY_NAME_DESC = "Car.findAllCarsSortedByNameDesc";

    @Column(nullable = false)
    private String brand;

    @Column(nullable = false)
    private String model;

    @Column(name = "year_of_manifacture", nullable = false)
    private Integer yearOfManifacture;

    @Column(name = "body_mass")
    private Integer bodyMass;

    @Column(name = "top_speed")
    private Integer topSpeed;

    @Column(name = "price_in_euro", nullable = false)
    private Integer priceInEuro;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "seller_id")
    private User seller;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "buyer_id")
    private User buyer;

    public Car() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getYearOfManifacture() {
        return yearOfManifacture;
    }

    public void setYearOfManifacture(Integer yearOfManifacture) {
        this.yearOfManifacture = yearOfManifacture;
    }

    public Integer getBodyMass() {
        return bodyMass;
    }

    public void setBodyMass(Integer bodyMass) {
        this.bodyMass = bodyMass;
    }

    public Integer getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(Integer topSpeed) {
        this.topSpeed = topSpeed;
    }

    public Integer getPriceInEuro() {
        return priceInEuro;
    }

    public void setPriceInEuro(Integer priceInEuro) {
        this.priceInEuro = priceInEuro;
    }

    public User getSeller() {
        return seller;
    }

    public void setSeller(User user) {
        this.seller = user;
    }

    public User getBuyer() {
        return buyer;
    }

    public void setBuyer(User buyer) {
        this.buyer = buyer;
    }
}
